# Online Sales Management System (Web-Application)
# About : 
- Description: A web application to manage online sales system
- Languages programing, libraries and frameworks : C#, ADO .Net Framework
- Role/Main Tasks : Individual/Backend

# My features: 
## 1. Customer Management (CRUD, Filter, Search, Sort)
#Requirnment: 
- List out the current Customer information. From this page, the user can link to the Customer Details page for adding new or updating a specific setting.
- Filters: Gender
- Search: by Name
- Columns: ID, Name, Birth Date, Gender, Address, Action, Sort Column
- Row Actions: Edit/Delete

Customer List
![1.1  Customer List](https://gitlab.com/hoangcmhe153602/prn211-onlinesalesmanagement/-/raw/main/Pictures/a1.PNG)

Customer Details
![1.2  Customer Details](https://gitlab.com/hoangcmhe153602/prn211-onlinesalesmanagement/-/raw/main/Pictures/a1b.PNG)

Customer Add
![1.3  Customer Add](https://gitlab.com/hoangcmhe153602/prn211-onlinesalesmanagement/-/raw/main/Pictures/a1a.PNG)

## 2. Product Management (CRUD, Filter, Search, Sort)
#Requirnment: 
- List out the current Product information. From this page, the user can link to the Product Details page for adding new or updating a specific setting.
- Filters: Category, Discontinued
- Search: by Name
- Columns: ID, Name, Unit Price, Unit in Stock, Image, Category, Discontinued, Action, Sort Column
- Row Actions: Edit/Delete

Product List
![1.1  Product List](https://gitlab.com/hoangcmhe153602/prn211-onlinesalesmanagement/-/raw/main/Pictures/b1.PNG)

Product Details
![1.2  Product Details](https://gitlab.com/hoangcmhe153602/prn211-onlinesalesmanagement/-/raw/main/Pictures/b1b.PNG)

Product Add
![1.3  Product Add](https://gitlab.com/hoangcmhe153602/prn211-onlinesalesmanagement/-/raw/main/Pictures/b1a.PNG)








